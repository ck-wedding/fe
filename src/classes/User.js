import axios from 'axios'

export default class User {
    constructor(obj) {
        this.setMemberVariable('name', 'name', obj);
        this.setMemberVariable('attending', 'attending', obj);
        this.setMemberVariable('songReq', 'songReq', obj);
        this.setMemberVariable('dietaries', 'dietaries', obj);
        this.setMemberVariable('addedBy', 'addedBy', obj);
    }

    setMemberVariable(classKey, objKey, obj) {
        if(Object.hasOwnProperty(obj,objKey)) {
            this[classKey] = obj[objKey];
        } else {
            this[classKey] = null;
        }
    }

    toJson() {
        return {
            full_name: this.name,
            attending: this.attending,
            dietary_requirements: this.dietaries,
            song_request: this.songReq,
            added_by: this.addedBy,
        };
    }

    static bulkSave(users) {
        let serialized_users = users.map( user => {
            return user.toJson();
        });
        return axios.post("http://api.wedding.com/api/guest", {'users' : serialized_users});
    }
};